#!/usr/bin/env bash
PASSWORD="12345678"

echo $PASSWORD | sudo -S qemu-system-riscv64 \
    -machine 'virt' \
    -cpu 'rv64' \
    -m 4G \
    -device virtio-blk-device,drive=hd \
    -drive file=/home/grainrain/root/pubcourse/EulixOS-train/2024-exercises-stage-1/2024-exercises-virtual-machines/stage-1.qcow2,if=none,id=hd \
    -virtfs local,id=lee,path=/home/grainrain/root/pubcourse/EulixOS-train/2024-exercises-stage-1,mount_tag=lee,security_model=passthrough \
    -bios /usr/lib/riscv64-linux-gnu/opensbi/generic/fw_jump.elf \
    -kernel /usr/lib/u-boot/qemu-riscv64_smode/uboot.elf \
    -object rng-random,filename=/dev/urandom,id=rng \
    -device virtio-rng-device,rng=rng \
    -nographic \
    -append "root=LABEL=rootfs console=ttyS0"
